package edu.msdamoredrexel.csproject.customlisteners;

import android.content.Context;
import android.util.Log;
import android.widget.ListView;

import com.android.volley.Response;
import com.cloudmine.api.CMObject;
import com.cloudmine.api.rest.response.CMObjectResponse;

import java.util.ArrayList;

import edu.msdamoredrexel.csproject.Patient;
import edu.msdamoredrexel.csproject.caseoftheweekactivity.PatientAdapter;

/**
 * Created by cheng on 3/16/2015.
 */
public class PatientCMResponseListener implements Response.Listener<CMObjectResponse> {
    private ArrayList<Patient> data;
    private Context context;
    private ListView list;

    public PatientCMResponseListener(Context context, ArrayList<Patient> data, ListView list) {
        this.context = context;
        this.data = data;
        this.list = list;
    }

    @Override
    public void onResponse(CMObjectResponse response) {
        for (CMObject patient : response.getObjects()) {
            if (!data.contains(patient)) {
                data.add((Patient) patient);
                Log.d("Added patient to list", ((Patient) patient).getName_());
            }
        }
        PatientAdapter adapter = new PatientAdapter(context, data);
        list.setAdapter(adapter);
    }
}
