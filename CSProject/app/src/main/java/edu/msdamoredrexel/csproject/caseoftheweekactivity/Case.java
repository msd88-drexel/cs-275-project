package edu.msdamoredrexel.csproject.caseoftheweekactivity;

import android.content.Context;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.widget.ListView;

import com.cloudmine.api.SearchQuery;
import com.cloudmine.api.db.LocallySavableCMObject;

import java.util.ArrayList;

import edu.msdamoredrexel.csproject.Patient;
import edu.msdamoredrexel.csproject.R;
import edu.msdamoredrexel.csproject.customlisteners.PatientCMResponseListener;


public class Case extends ActionBarActivity {
    public static Context parentContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_case);
        parentContext = this;

        final ArrayList<Patient> data = new ArrayList<Patient>();
        ListView ip_listView1 = (ListView) findViewById(R.id.ip_listView1);
        SearchQuery workerSearch = new SearchQuery();
        workerSearch.filter(Patient.class);
        LocallySavableCMObject.searchObjects(this, SearchQuery.filter("__class__").equal("edu.msdamoredrexel.csproject.Patient").searchQuery(),
                new PatientCMResponseListener(this, data, ip_listView1));
    }

}






