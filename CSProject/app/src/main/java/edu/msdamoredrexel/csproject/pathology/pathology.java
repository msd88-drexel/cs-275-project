package edu.msdamoredrexel.csproject.pathology;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.cloudmine.api.SearchQuery;
import com.cloudmine.api.db.LocallySavableCMObject;

import java.util.HashMap;
import java.util.Map;

import edu.msdamoredrexel.csproject.R;
import edu.msdamoredrexel.csproject.Worker;
import edu.msdamoredrexel.csproject.customlisteners.DiseaseCMResponseListener;
import edu.msdamoredrexel.csproject.customlisteners.SearchButtonOnClickListener;


public class pathology extends ActionBarActivity {
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pathology);
        TextView label_name = (TextView) findViewById(R.id.label_name);
        label_name.setVisibility(View.INVISIBLE);
        TextView label_symp = (TextView) findViewById(R.id.label_symp);
        label_symp.setVisibility(View.INVISIBLE);
        TextView label_ma = (TextView) findViewById(R.id.label_management);
        label_ma.setVisibility(View.INVISIBLE);
        ImageView image = (ImageView) findViewById(R.id.path_img);
        image.setVisibility(View.INVISIBLE);
        AutoCompleteTextView myAutoComplete = (AutoCompleteTextView) findViewById(R.id.myautocomplete);

        SearchQuery workerSearch = new SearchQuery();
        workerSearch.filter(Worker.class);
        LocallySavableCMObject.searchObjects(this, SearchQuery.filter("__class__").equal("edu.msdamoredrexel.csproject.Disease").searchQuery(),
                new DiseaseCMResponseListener(this, myAutoComplete));
        Log.d("testing", "the response was sent");

        Map<String, View> views = new HashMap<>();

        views.put("image", image);
        views.put("label_name", label_name);
        views.put("label_symp", label_symp);
        views.put("label_ma", label_ma);
        views.put("name", findViewById(R.id.text_name));
        views.put("symp", findViewById(R.id.text_symp));
        views.put("ma", findViewById(R.id.text_ma));
        views.put("not_found", findViewById(R.id.not_found));

        if (myAutoComplete == null) { Log.d("Check", "Is Null"); } else { Log.d("Check", "Is Not Null"); }

        ImageButton k = (ImageButton) findViewById(R.id.search_button);
        k.setOnClickListener(new SearchButtonOnClickListener(this, myAutoComplete, views));
    }
}
