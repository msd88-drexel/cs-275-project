package edu.msdamoredrexel.csproject.customlisteners;

import android.view.View;

/**
 * Created by Khang on 3/15/2015.
 */
public class ViewVisibilityOnClickListener implements View.OnClickListener {
    private View hidden_view;

    public ViewVisibilityOnClickListener(View hidden_view) {
        this.hidden_view = hidden_view;
    }

    @Override
    public void onClick(View v) {
        if (hidden_view.getVisibility() == View.GONE) {
            hidden_view.setVisibility(View.VISIBLE);
        }
        else if (hidden_view.getVisibility() == View.VISIBLE) {
            hidden_view.setVisibility(View.GONE);
        }
    }
}
