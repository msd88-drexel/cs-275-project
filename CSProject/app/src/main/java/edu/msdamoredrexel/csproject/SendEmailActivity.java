package edu.msdamoredrexel.csproject;

import android.support.v7.app.ActionBarActivity;

import android.os.Bundle;
import android.content.Intent;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

public class SendEmailActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_email);
        Intent intent = getIntent();
        EditText edit_to = (EditText) findViewById(R.id.edit_to);
        edit_to.setText(intent.getStringExtra("Recipient"));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            Toast.makeText(this, "Email sent.", Toast.LENGTH_SHORT).show();
            Intent intent = new Intent();
            setResult(RESULT_OK, intent);
            finish();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_send_email, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case R.id.action_send:
                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.setType("message/rfc822");
                Intent parent_intent = getIntent();

                EditText edit_to = (EditText) findViewById(R.id.edit_to);
                String to = edit_to.getText().toString();
                EditText edit_cc = (EditText) findViewById(R.id.edit_cc);
                String cc = edit_cc.getText().toString();
                EditText edit_subject = (EditText) findViewById(R.id.edit_subject);
                String subject = edit_subject.getText().toString();
                EditText edit_message = (EditText) findViewById(R.id.edit_message);
                String message = edit_message.getText().toString();

                intent.putExtra(Intent.EXTRA_EMAIL, new String[]{to});
                intent.putExtra(Intent.EXTRA_CC, cc);
                intent.putExtra(Intent.EXTRA_SUBJECT, subject);
                intent.putExtra(Intent.EXTRA_TEXT, message);
                try {
                    startActivityForResult(Intent.createChooser(intent, "Choose an email client"), 1);
                } catch (android.content.ActivityNotFoundException ex) {
                    Toast.makeText(this, "There are no email clients installed.", Toast.LENGTH_SHORT).show();
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
