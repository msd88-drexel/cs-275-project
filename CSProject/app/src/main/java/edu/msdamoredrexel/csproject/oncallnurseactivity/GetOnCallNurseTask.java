package edu.msdamoredrexel.csproject.oncallnurseactivity;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Response;
import com.cloudmine.api.SearchQuery;
import com.cloudmine.api.db.LocallySavableCMObject;
import com.cloudmine.api.rest.response.CMObjectResponse;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.temboo.Library.Google.Calendar.GetAllEvents;
import com.temboo.Library.Google.Calendar.SearchCalendarsByName;
import com.temboo.core.TembooException;
import com.temboo.core.TembooSession;

import org.joda.time.DateTime;

import java.util.Map;

import edu.msdamoredrexel.csproject.BitmapProcessingTask;
import edu.msdamoredrexel.csproject.Worker;
import edu.msdamoredrexel.csproject.customlisteners.CallOnClickListener;
import edu.msdamoredrexel.csproject.customlisteners.MailButtonOnClickListener;

/**
 * Created by Matthew Damore on 3/15/2015.
 */
public class GetOnCallNurseTask extends AsyncTask<Void, Void, String> {

    private String accessToken;
    private Context context;
    private Map<String, View> layoutInfo;

    public GetOnCallNurseTask(String accessToken, Context context, Map<String, View> layoutInfo){
        this.accessToken = accessToken;
        this.context = context;
        this.layoutInfo = layoutInfo;
    }


    @Override
    protected String doInBackground(Void... params) {
        try {
            // Get calendar id for the onCallNurse calendar to access the event members and get the correct nurse
            TembooSession session = new TembooSession("msd88", "myFirstApp", "738da4a6d1064ccfa72cacd3390547e9");
            SearchCalendarsByName searchCalendarsByNameChoreo = new SearchCalendarsByName(session);
            // Get an InputSet object for the choreo
            SearchCalendarsByName.SearchCalendarsByNameInputSet searchCalendarsByNameInputs = searchCalendarsByNameChoreo.newInputSet();
            // Set inputs
            searchCalendarsByNameInputs.set_CalendarName("OnCallNurse");
            searchCalendarsByNameInputs.set_AccessToken(accessToken);
            SearchCalendarsByName.SearchCalendarsByNameResultSet searchCalendarsByNameResults = searchCalendarsByNameChoreo.execute(searchCalendarsByNameInputs);
            String calendarId = searchCalendarsByNameResults.get_CalendarId();
            Log.d("calendar", calendarId);

            GetAllEvents getAllEventsChoreo = new GetAllEvents(session);

            // Get an InputSet object for the choreo
            GetAllEvents.GetAllEventsInputSet getAllEventsInputs = getAllEventsChoreo.newInputSet();

            // Set inputs
            getAllEventsInputs.set_AccessToken(accessToken);
            getAllEventsInputs.set_CalendarID(calendarId);

            // Execute Choreo
            GetAllEvents.GetAllEventsResultSet getAllEventsResults = getAllEventsChoreo.execute(getAllEventsInputs);

            JsonParser jsonParser = new JsonParser();
            JsonObject root = (JsonObject) jsonParser.parse(getAllEventsResults.get_Response());

            JsonArray items = root.getAsJsonArray("items");

            for(JsonElement eventElement : items){
                JsonObject eventObject = eventElement.getAsJsonObject();

                String   workerName  = eventObject.get("summary").getAsString();
                DateTime shiftStart  = new DateTime(eventObject.getAsJsonObject("start").get("dateTime").getAsString());
                DateTime shiftEnd    = new DateTime(eventObject.getAsJsonObject("end").get("dateTime").getAsString());
                DateTime currentTime = new DateTime();

                //logic to determine if the current time lies within the range in which this current worker's shift lies
                if( ( currentTime.compareTo(shiftStart) > 0 && currentTime.compareTo(shiftEnd) < 0 ) || currentTime.compareTo(shiftEnd) == 0 || currentTime.compareTo(shiftStart) == 0){
                    return workerName;
                }
            }
        } catch (TembooException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(String workerName) {
        super.onPostExecute(workerName);
        //search database for the nurse that matches the name received from the google calendar
        LocallySavableCMObject.searchObjects(context, SearchQuery.filter("name").equal(workerName).searchQuery(),
        new Response.Listener<CMObjectResponse>() {
            @Override
            public void onResponse(CMObjectResponse response) {

                //If there is a response from he server
                if (response.getObjects().size() != 0) {
                    Worker workerResponse = (Worker) response.getObjects().get(0);

                    //set text and buttons to the information stored in the worker object.
                    ((TextView) layoutInfo.get("workerNameView")).setText(workerResponse.getName());
                    ((TextView) layoutInfo.get("workerPositionView")).setText(workerResponse.getPosition());
                    ((TextView) layoutInfo.get("workerNumberView")).setText(workerResponse.getNumber());
                    ((TextView) layoutInfo.get("workerEmailView")).setText(workerResponse.getEmail());

                    (layoutInfo.get("callNurse")).setOnClickListener(new CallOnClickListener(workerResponse.getNumber(), context));
                    (layoutInfo.get("emailNurse")).setOnClickListener(new MailButtonOnClickListener(workerResponse.getEmail(), context));

                    ImageView workerImage = ((ImageView) layoutInfo.get("workerImageView"));

                    //If there is an image string to be decoded in the worker object response
                    if (workerResponse.getImage() == null) {
                        workerImage.setImageBitmap(null);
                    } else {
                        byte[] bytes = Base64.decode(workerResponse.getImage(), Base64.DEFAULT);
                        BitmapProcessingTask.loadBitmap(BitmapFactory.decodeByteArray(bytes, 0,
                                bytes.length), workerImage);
                    }
                }
                else{
                    //Fill in some default info if the server does not respond with anything
                    ((TextView) layoutInfo.get("workerNameView")).setText("No on call nurse found! Check Calendar");
                    ( layoutInfo.get("callNurse")).setVisibility(View.GONE);
                    ( layoutInfo.get("emailNurse")).setVisibility(View.GONE);
                }
            }
        });
    }
}
