package edu.msdamoredrexel.csproject.customlisteners;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Response;
import com.cloudmine.api.CMObject;
import com.cloudmine.api.SearchQuery;
import com.cloudmine.api.db.LocallySavableCMObject;
import com.cloudmine.api.rest.response.CMObjectResponse;

import java.util.Map;

import edu.msdamoredrexel.csproject.Disease;
import edu.msdamoredrexel.csproject.pathology.GetBitmapFromUrlTask;

/**
 * Created by Khang on 3/16/2015.
 */
public class SearchButtonOnClickListener implements View.OnClickListener{
    private Context context;
    private Map<String, View> views;
    private AutoCompleteTextView myAutocomplete;

    public SearchButtonOnClickListener(Context context, AutoCompleteTextView myAutoComplete, Map<String, View> views) {
        this.myAutocomplete = myAutoComplete;
        this.context = context;
        this.views = views;
    }

    @Override
    public void onClick(View v) {
        String disease_name = myAutocomplete.getText().toString();
        LocallySavableCMObject.searchObjects(context, SearchQuery.filter("name").equal(disease_name).searchQuery(),
            new Response.Listener<CMObjectResponse>() {
                @Override
                public void onResponse(CMObjectResponse response) {
                    Disease o = null;
                    for (CMObject disease : response.getObjects()) {
                        o = ((Disease) disease);
                        Log.d("added disease", ((Disease) disease).getName());
                    }
                    if (o != null) {
                        ImageView picture = (ImageView) views.get("image");
                        picture.setVisibility(View.VISIBLE);
                        GetBitmapFromUrlTask getImage = new GetBitmapFromUrlTask(o.getImage(), picture);
                        getImage.execute();
                        TextView not_found = (TextView) views.get("not_found");
                        not_found.setVisibility(View.INVISIBLE);
                        TextView label_Name = (TextView) views.get("label_name");
                        label_Name.setVisibility(View.VISIBLE);
                        TextView label_Sy = (TextView) views.get("label_symp");
                        label_Sy.setVisibility(View.VISIBLE);
                        TextView label_Ma = (TextView) views.get("label_ma");
                        label_Ma.setVisibility(View.VISIBLE);
                        TextView name = (TextView) views.get("name");
                        name.setText(o.getName());
                        TextView sy = (TextView) views.get("symp");
                        sy.setText(o.getSymptoms());
                        TextView ma = (TextView) views.get("ma");
                        ma.setText(o.getManagement());
                    } else {
                        TextView not_found = (TextView) views.get("not_found");
                        not_found.setVisibility(View.VISIBLE);
                        ImageView picture = (ImageView) views.get("image");
                        picture.setVisibility(View.INVISIBLE);
                        TextView label_Name = (TextView) views.get("label_name");
                        label_Name.setVisibility(View.INVISIBLE);
                        TextView label_Sy = (TextView) views.get("label_symp");
                        label_Sy.setVisibility(View.INVISIBLE);
                        TextView label_Ma = (TextView) views.get("label_ma");
                        label_Ma.setVisibility(View.INVISIBLE);
                        TextView name = (TextView) views.get("name");
                        name.setText("");
                        TextView sy = (TextView) views.get("symp");
                        sy.setText("");
                        TextView ma = (TextView) views.get("ma");
                        ma.setText("");
                    }
                }
            });
    }
}
