package edu.msdamoredrexel.csproject.personnelactivity;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

import edu.msdamoredrexel.csproject.BitmapProcessingTask;
import edu.msdamoredrexel.csproject.R;
import edu.msdamoredrexel.csproject.Worker;
import edu.msdamoredrexel.csproject.customlisteners.CallOnClickListener;
import edu.msdamoredrexel.csproject.customlisteners.MailButtonOnClickListener;
import edu.msdamoredrexel.csproject.customlisteners.ViewVisibilityOnClickListener;

/**
 * Created by Khang on 3/13/2015.
 */
public class PersonnelAdapter extends ArrayAdapter<Worker> {
    private Context context;

    public PersonnelAdapter(Context context, ArrayList<Worker> data) {
        super(context, R.layout.ip_list_item, data);
        this.context = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        Worker worker = getItem(position);
        if(convertView == null) {
            LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(
                    Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.ip_list_item, parent, false);
        }

        TextView personnel_name = (TextView) convertView.findViewById(R.id.personnel_name);
        personnel_name.setText(worker.getName());

        TextView personnel_position = (TextView) convertView.findViewById(R.id.personnel_position);
        personnel_position.setText(worker.getPosition());

        TextView personnel_number = (TextView) convertView.findViewById(R.id.personnel_number);
        personnel_number.setText(worker.getNumber());

        TextView personnel_email = (TextView) convertView.findViewById(R.id.personnel_email);
        personnel_email.setText(worker.getEmail());

        ImageView personnel_img = (ImageView) convertView.findViewById(R.id.personnel_img);
        if (worker.getImage() == null) {
            personnel_img.setImageBitmap(null);
        } else {
            byte[] bytes = Base64.decode(worker.getImage(), Base64.DEFAULT);
            BitmapProcessingTask.loadBitmap(BitmapFactory.decodeByteArray(bytes, 0,
                    bytes.length), personnel_img);
        }

        RelativeLayout contact_buttons = (RelativeLayout) convertView.findViewById(R.id.contact_buttons);
        contact_buttons.setVisibility(View.GONE);

        Button call_button = (Button) contact_buttons.findViewById(R.id.call_button);
        call_button.setOnClickListener(new CallOnClickListener(worker.getNumber(), context));

        Button email_button = (Button) contact_buttons.findViewById(R.id.email_button);
        email_button.setOnClickListener(new MailButtonOnClickListener(worker.getEmail(), context));

        RelativeLayout worker_info = (RelativeLayout) convertView.findViewById(R.id.worker_info);
        worker_info.setOnClickListener(new ViewVisibilityOnClickListener(contact_buttons));

        return convertView;
    }
}