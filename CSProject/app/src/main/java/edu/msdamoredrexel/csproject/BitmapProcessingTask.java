package edu.msdamoredrexel.csproject;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.util.Base64;
import android.widget.ImageView;

import java.io.ByteArrayOutputStream;
import java.lang.ref.WeakReference;

/**
 * Created by Khang on 3/16/2015.
 */
public class BitmapProcessingTask extends AsyncTask<Void, Void, Bitmap> {
    private final WeakReference<ImageView> imageViewReference;
    private Bitmap bitmap = null;
    private String bitmapData = "";

    public BitmapProcessingTask(ImageView imageView, Bitmap bitmap) {
        imageViewReference = new WeakReference<ImageView>(imageView);
        this.bitmap = bitmap;
        if (bitmap != null) {
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
            bitmapData = Base64.encodeToString(bytes.toByteArray(), Base64.DEFAULT);
        }
    }

    @Override
    protected Bitmap doInBackground(Void... params) {
        if (bitmap != null) {
            int width = bitmap.getWidth();
            int height = bitmap.getHeight();
            if (width > height) {
                height = width;
            } else {
                width = height;
            }

            bitmap = Bitmap.createScaledBitmap(bitmap, width, height, true);

            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;

            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes);

            BitmapFactory.decodeByteArray(bytes.toByteArray(), 0, bytes.toByteArray().length, options);

            int inSampleSize = 1;
            int view_height = imageViewReference.get().getHeight();
            int view_width = imageViewReference.get().getWidth();
            if (height > view_height || width > view_width) {
                int halfHeight = height / 2;
                int halfWidth = width / 2;

                // Calculate the largest inSampleSize value that is a power of 2 and keeps both
                // height and width larger than the requested height and width.
                while ((halfHeight / inSampleSize) > view_height
                        && (halfWidth / inSampleSize) > view_width) {
                    inSampleSize *= 2;
                }
            }

            options.inJustDecodeBounds = false;
            options.inSampleSize = inSampleSize;

            return BitmapFactory.decodeByteArray(bytes.toByteArray(), 0, bytes.toByteArray().length, options);
        }
        return null;
    }

    @Override
    protected void onPostExecute(Bitmap bitmap) {
        super.onPostExecute(bitmap);
        if (isCancelled()) {
            bitmap = null;
        }

        if (imageViewReference != null && bitmap != null) {
            final ImageView imageView = imageViewReference.get();
            if (imageView != null) {
                imageView.setImageBitmap(bitmap);
            }
        }
    }

    static class AsyncDrawable extends BitmapDrawable {
        private final WeakReference<BitmapProcessingTask> bitmapTaskReference;

        public AsyncDrawable(BitmapProcessingTask bitmapTask) {
            bitmapTaskReference = new WeakReference<BitmapProcessingTask>(bitmapTask);
        }

        public BitmapProcessingTask getBitmapTask() {
            return bitmapTaskReference.get();
        }
    }

    private static BitmapProcessingTask getBitmapTask(ImageView imageView) {
        if (imageView != null) {
            final Drawable drawable = imageView.getDrawable();
            if (drawable instanceof AsyncDrawable) {
                final AsyncDrawable asyncDrawable = (AsyncDrawable) drawable;
                return asyncDrawable.getBitmapTask();
            }
        }
        return null;
    }

    public static boolean cancelPotentialWork(Bitmap bitmap, ImageView imageView) {
        final BitmapProcessingTask bitmapTask = getBitmapTask(imageView);

        if (bitmapTask != null) {
            final String bitmapData = bitmapTask.bitmapData;
            String bitmapStr = "";
            if (bitmap != null) {
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
                bitmapStr = Base64.encodeToString(bytes.toByteArray(), Base64.DEFAULT);
            }
            // If bitmapData is not yet set or it differs from the new data
            if (bitmapData.equals("") || bitmapData != bitmapStr) {
                // Cancel previous task
                bitmapTask.cancel(true);
            } else {
                // The same work is already in progress
                return false;
            }
        }
        // No task associated with the ImageView, or an existing task was cancelled
        return true;
    }

    public static void loadBitmap(Bitmap bitmap, ImageView imageView) {
        if (cancelPotentialWork(bitmap, imageView)) {
            final BitmapProcessingTask task = new BitmapProcessingTask(imageView, bitmap);
            final AsyncDrawable asyncDrawable = new AsyncDrawable(task);
            imageView.setImageDrawable(asyncDrawable);
            task.execute();
        }
    }
}