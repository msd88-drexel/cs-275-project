package edu.msdamoredrexel.csproject;

import com.cloudmine.api.db.LocallySavableCMObject;

/**
 * Created by cheng on 3/15/2015.
 */
public class Disease extends LocallySavableCMObject {
    private String name;
    private String symptoms;
    private String management;
    private String image;

    public Disease(String name, String symptoms, String Management, String image) {
        this.name = name;
        this.management=Management;
        this.symptoms=symptoms;
        this.image = image;

    }

    //Adding empty constructor for cloudmine
    public Disease(){}

    public String getSymptoms() { return symptoms; }

    public String getName() {
        return name;
    }

    public String getManagement() { return management; }

    public String getImage() {
        return image;
    }

}
