package edu.msdamoredrexel.csproject.customlisteners;

import android.content.Context;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;

import com.android.volley.Response;
import com.cloudmine.api.CMObject;
import com.cloudmine.api.rest.response.CMObjectResponse;

import java.util.ArrayList;

import edu.msdamoredrexel.csproject.Disease;

/**
 * Created by Khang on 3/16/2015.
 */
public class DiseaseCMResponseListener implements Response.Listener<CMObjectResponse> {
    private Context context;
    private AutoCompleteTextView auto;

    public DiseaseCMResponseListener(Context context, AutoCompleteTextView auto) {
        this.context = context;
        this.auto = auto;
    }

    @Override
    public void onResponse(CMObjectResponse response) {
        ArrayList<Disease> data = new ArrayList<>();
        for(CMObject disease : response.getObjects()) {
            data.add((Disease) disease);
            Log.d("added worker", ((Disease) disease).getName());
        }
        String item[] = new String[response.getObjects().size()];
        for (int p = 0; p < data.size(); p++) {
            item[p] = (data.get(p).getName());
        }
        auto.setAdapter(new ArrayAdapter<String>(context, android.R.layout.simple_dropdown_item_1line, item));
    }
}
