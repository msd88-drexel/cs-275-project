package edu.msdamoredrexel.csproject.personnelactivity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.ActionBarActivity;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.cloudmine.api.rest.response.ObjectModificationResponse;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import edu.msdamoredrexel.csproject.BitmapProcessingTask;
import edu.msdamoredrexel.csproject.R;
import edu.msdamoredrexel.csproject.Worker;


public class AddWorkerActivity extends ActionBarActivity {

    public static final String TAG = "ADD_WORKER";

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            if (resultCode == RESULT_OK) {
                ImageView worker_img = (ImageView) findViewById(R.id.worker_img);
                Uri chosenImageUri = data.getData();

                Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), chosenImageUri);

                BitmapProcessingTask task = new BitmapProcessingTask(worker_img, bitmap);
                task.execute();
            }
        } catch (IOException e) {
            Log.e("ERROR", e.toString());
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_worker);

        Button upload_img = (Button) findViewById(R.id.upload_img);
        upload_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent photoPickerIntent = new Intent(Intent.ACTION_GET_CONTENT);
                photoPickerIntent.setType("image/*");
                startActivityForResult(photoPickerIntent, 1);
            }
        });

        Button add_worker = (Button) findViewById(R.id.add_worker_button);
        add_worker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText edit_name = (EditText) findViewById(R.id.edit_worker_name);
                String name = edit_name.getText().toString();
                EditText edit_pos = (EditText) findViewById(R.id.edit_worker_pos);
                String pos = edit_pos.getText().toString();
                EditText edit_num = (EditText) findViewById(R.id.edit_worker_num);
                String num = edit_num.getText().toString();
                EditText edit_email = (EditText) findViewById(R.id.edit_worker_email);
                String email = edit_email.getText().toString();
                ImageView image = (ImageView) findViewById(R.id.worker_img);
                Bitmap bmp = ((BitmapDrawable) image.getDrawable()).getBitmap();
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                bmp.compress(Bitmap.CompressFormat.JPEG, 100, bytes);

                String img = Base64.encodeToString(bytes.toByteArray(), Base64.DEFAULT);
                Worker worker = new Worker(name, pos, num, email, img);

                worker.save(ImportantPersonnelActivity.parentContext, new Response.Listener<ObjectModificationResponse>() {
                    @Override
                    public void onResponse(ObjectModificationResponse modificationResponse) {
                        Log.d(TAG, "Worker was saved: " + modificationResponse.getCreatedObjectIds());
                        Intent intent = new Intent();
                        setResult(RESULT_OK, intent);
                        finish();
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        Log.e(TAG, "Failed saving worker", volleyError);
                        Intent intent = new Intent();
                        setResult(RESULT_CANCELED, intent);
                        finish();
                    }
                });
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_add_worker, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return super.onOptionsItemSelected(item);
    }
}