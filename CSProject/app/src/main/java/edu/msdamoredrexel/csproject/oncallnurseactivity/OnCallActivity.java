package edu.msdamoredrexel.csproject.oncallnurseactivity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;

import java.util.HashMap;
import java.util.Map;

import edu.msdamoredrexel.csproject.R;

/**
 * Created by damor_000 on 3/14/2015.
 */
public class OnCallActivity extends ActionBarActivity{

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.on_call_layout);

        Intent intent = getIntent();

        final String accessToken = intent.getStringExtra("accessToken");
        Map<String, View> viewObjects = new HashMap<>();

        viewObjects.put( "workerNameView" , findViewById(R.id.onCallNurseName));
        viewObjects.put( "workerPositionView" ,  findViewById(R.id.onCallPosition));
        viewObjects.put( "workerNumberView" ,  findViewById(R.id.onCallNumber));
        viewObjects.put( "workerImageView" ,  findViewById(R.id.workerImageView));
        viewObjects.put( "workerEmailView" , findViewById(R.id.onCallEmail));
        viewObjects.put( "callNurse" ,  findViewById(R.id.callOnCallNurse));
        viewObjects.put( "emailNurse" , findViewById(R.id.emailOnCallNurse));

        GetOnCallNurseTask getOnCallNurseTask = new GetOnCallNurseTask(accessToken, this, viewObjects);
        getOnCallNurseTask.execute();
    }

    @Override
    protected void onRestart() {
        super.onRestart();

        Intent intent = getIntent();

        final String accessToken = intent.getStringExtra("accessToken");
        Map<String, View> viewObjects = new HashMap<>();

        viewObjects.put( "workerNameView" , findViewById(R.id.onCallNurseName));
        viewObjects.put( "workerPositionView" ,  findViewById(R.id.onCallPosition));
        viewObjects.put( "workerNumberView" ,  findViewById(R.id.onCallNumber));
        viewObjects.put( "workerImageView" ,  findViewById(R.id.workerImageView));
        viewObjects.put( "workerEmailView" , findViewById(R.id.onCallEmail));
        viewObjects.put( "callNurse" ,  findViewById(R.id.callOnCallNurse));
        viewObjects.put( "emailNurse" , findViewById(R.id.emailOnCallNurse));

        GetOnCallNurseTask getOnCallNurseTask = new GetOnCallNurseTask(accessToken, this, viewObjects);
        getOnCallNurseTask.execute();
    }
}
