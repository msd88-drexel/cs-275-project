package edu.msdamoredrexel.csproject.caseoftheweekactivity;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

import edu.msdamoredrexel.csproject.Patient;
import edu.msdamoredrexel.csproject.R;
import edu.msdamoredrexel.csproject.customlisteners.ViewVisibilityOnClickListener;

/**
 * Created by cheng on 3/16/2015.
 */
public class PatientAdapter extends ArrayAdapter<Patient> {

    public PatientAdapter(Context context, ArrayList<Patient> data) {
        super(context, R.layout.patient_list, data);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        Patient patient = getItem(position);
        if(convertView == null) {
            LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(
                    Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.patient_list, parent, false);
        }

        TextView patient_name = (TextView) convertView.findViewById(R.id.patient_name);
        patient_name.setText(patient.getName_());

        TextView patient_date = (TextView) convertView.findViewById(R.id.patient_date);
        patient_date.setText(patient.getDate_());
        TextView patient_health = (TextView) convertView.findViewById(R.id.patient_health);
        patient_health.setText(patient.getHealth_());
        TextView patient_qol = (TextView) convertView.findViewById(R.id.patient_qol);
        patient_qol.setText(patient.getQualityoflife_());
        RelativeLayout extend = (RelativeLayout) convertView.findViewById(R.id.extend_info);
        extend.setVisibility(View.GONE);
        TextView patient_ghp = (TextView) convertView.findViewById(R.id.patient_ghp);
        patient_ghp.setText(patient.getGeneralhealthperceptions_());
        TextView patient_dis = (TextView) convertView.findViewById(R.id.patient_dis);
        patient_dis.setText(patient.getDisability_());
        TextView patient_symp = (TextView) convertView.findViewById(R.id.patient_symp);
        patient_symp.setText(patient.getSymptom_());

        RelativeLayout patient_info = (RelativeLayout) convertView.findViewById(R.id.patient_info);
        patient_info.setOnClickListener(new ViewVisibilityOnClickListener(extend));

        return convertView;
    }
}
