package edu.msdamoredrexel.csproject;

import com.cloudmine.api.db.LocallySavableCMObject;

/**
 * Created by Khang on 3/13/2015.
 */
public class Worker extends LocallySavableCMObject {
    private String name;
    private String position;
    private String number;
    private String email;
    private String image;

    public Worker(String name, String position, String number, String email, String image) {
        this.name = name;
        this.position = position;
        this.number = number;
        this.email = email;
        this.image = image;
    }
    //Added empty constructor for cloudmine
    public Worker(){

    }

    public String getNumber() {
        return number;
    }

    public String getName() {
        return name;
    }

    public String getPosition() {
        return position;
    }

    public String getEmail() {
        return email;
    }

    public String getImage() { return image; }
}
