package edu.msdamoredrexel.csproject.customlisteners;

import android.content.Context;
import android.util.Log;
import android.widget.ListView;

import com.android.volley.Response;
import com.cloudmine.api.CMObject;
import com.cloudmine.api.rest.response.CMObjectResponse;

import java.util.ArrayList;

import edu.msdamoredrexel.csproject.Worker;
import edu.msdamoredrexel.csproject.personnelactivity.PersonnelAdapter;

/**
 * Created by Khang on 3/16/2015.
 */
public class PersonnelCMResponseListener implements Response.Listener<CMObjectResponse> {
    private ArrayList<Worker> data;
    private Context context;
    private ListView list;

    public PersonnelCMResponseListener(Context context, ArrayList<Worker> data, ListView list) {
        this.context = context;
        this.data = data;
        this.list = list;
    }

    @Override
    public void onResponse(CMObjectResponse response) {
        for (CMObject worker : response.getObjects()) {
            if (!data.contains(worker)) {
                data.add((Worker) worker);
                Log.d("Added worker to list", ((Worker) worker).getName());
            }
        }
        PersonnelAdapter adapter = new PersonnelAdapter(context, data);
        list.setAdapter(adapter);
    }
}
