package edu.msdamoredrexel.csproject;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;

import com.cloudmine.api.CMApiCredentials;

import edu.msdamoredrexel.csproject.caseoftheweekactivity.Case;
import edu.msdamoredrexel.csproject.googleoauth.InitilizeOauth;
import edu.msdamoredrexel.csproject.googleoauth.UserOauthActivity;
import edu.msdamoredrexel.csproject.pathology.pathology;
import edu.msdamoredrexel.csproject.personnelactivity.ImportantPersonnelActivity;


public class MainActivity extends ActionBarActivity {
    private static final String APP_ID = "7bca10af3fda4494b9851de271e75905";
    private static final String API_KEY = "25DA93A10D164369A75008E2451A0DA7";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        CMApiCredentials.initialize(APP_ID, API_KEY, getApplicationContext());

        LinearLayout important_personnel_button = (LinearLayout) findViewById(R.id.important_num);
        LinearLayout on_call_button = (LinearLayout) findViewById(R.id.on_call);
        LinearLayout pathology_manual_button=(LinearLayout) findViewById(R.id.pathology);
        LinearLayout case_of_week_button=(LinearLayout)findViewById(R.id.week_cases);

        important_personnel_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, ImportantPersonnelActivity.class);
                startActivity(intent);
            }
        });

        on_call_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, UserOauthActivity.class);
                InitilizeOauth initilizeOauth = new InitilizeOauth(intent, MainActivity.this);
                initilizeOauth.execute();
            }
        });

        pathology_manual_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, pathology.class);
                startActivity(intent);
            }
        });

        case_of_week_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, Case.class);
                startActivity(intent);
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return super.onOptionsItemSelected(item);
    }
}
