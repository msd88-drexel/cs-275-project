package edu.msdamoredrexel.csproject.personnelactivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;

import com.android.volley.Response;
import com.cloudmine.api.CMObject;
import com.cloudmine.api.SearchQuery;
import com.cloudmine.api.db.LocallySavableCMObject;
import com.cloudmine.api.rest.response.CMObjectResponse;

import java.util.ArrayList;

import edu.msdamoredrexel.csproject.R;
import edu.msdamoredrexel.csproject.Worker;
import edu.msdamoredrexel.csproject.customlisteners.PersonnelCMResponseListener;


public class ImportantPersonnelActivity extends ActionBarActivity {

    public static Context parentContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_important_personnel);

        parentContext = this;

        final ArrayList<Worker> data = new ArrayList<Worker>();
        SearchQuery workerSearch = new SearchQuery();
        workerSearch.filter(Worker.class);
        LocallySavableCMObject.searchObjects(this, SearchQuery.filter("__class__").equal("edu.msdamoredrexel.csproject.Worker").searchQuery(),
            new Response.Listener<CMObjectResponse>() {
                @Override
                public void onResponse(CMObjectResponse response) {
                    for(CMObject worker : response.getObjects()) {
                        data.add((Worker) worker);
                        Log.d("added worker", ((Worker) worker).getName());
                    }
                    ListView list = (ListView) findViewById(R.id.ip_listView);
                    PersonnelAdapter adapter = new PersonnelAdapter(ImportantPersonnelActivity.this, data);
                    list.setAdapter(adapter);
                }
            });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_important_personnel, menu);
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            if (resultCode == RESULT_OK) {
                ArrayList<Worker> workers = new ArrayList<Worker>();
                ListView ip_listView = (ListView) findViewById(R.id.ip_listView);
                SearchQuery workerSearch = new SearchQuery();
                workerSearch.filter(Worker.class);
                LocallySavableCMObject.searchObjects(this, SearchQuery.filter("__class__").equal("edu.msdamoredrexel.csproject.Worker").searchQuery(),
                        new PersonnelCMResponseListener(this, workers, ip_listView));
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case R.id.action_add:
                Intent intent = new Intent(ImportantPersonnelActivity.this, AddWorkerActivity.class);
                startActivityForResult(intent, 1);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
