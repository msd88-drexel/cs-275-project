package edu.msdamoredrexel.csproject.customlisteners;

import android.content.Context;
import android.content.Intent;
import android.view.View;

import edu.msdamoredrexel.csproject.SendEmailActivity;

/**
 * Created by Khang on 3/15/2015.
 */
public class MailButtonOnClickListener implements View.OnClickListener{
    private String recipient;
    private Context context;

    public MailButtonOnClickListener(String recipient, Context context) {
        this.recipient = recipient;
        this.context = context;
    }

    @Override
    public void onClick(View v) {
        if (recipient != "") {
            Intent intent = new Intent(context, SendEmailActivity.class);
            intent.putExtra("Recipient", recipient);
            context.startActivity(intent);
        }
    }
}
