package edu.msdamoredrexel.csproject.customlisteners;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.View;

/**
 * Created by Khang on 3/14/2015.
 */
public class CallOnClickListener implements View.OnClickListener{
    private String number;
    private Context context;

    public CallOnClickListener(String number, Context context) {
        this.number = number;
        this.context = context;
    }

    @Override
    public void onClick(View v) {
        if (number != "") {
            Intent intent = new Intent(Intent.ACTION_CALL);
            intent.setData(Uri.parse("tel:" + number));
            context.startActivity(intent);
        }
    }
}
