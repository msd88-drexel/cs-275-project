package edu.msdamoredrexel.csproject.googleoauth;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import edu.msdamoredrexel.csproject.R;
import edu.msdamoredrexel.csproject.oncallnurseactivity.OnCallActivity;

/**
 * Created by damor_000 on 3/14/2015.
 */
public class UserOauthActivity extends ActionBarActivity{

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.oauth_web_view);

        final Intent intent = getIntent();
        final String callbackId = intent.getStringExtra("callbackId");

        WebView webView = (WebView) findViewById(R.id.oauthWebview);
        // Set up WebView for OAuth2 login - intercept redirect when the redirect
        // URL matches our FORWARDING_URL, in which case we will complete the OAuth
        // flow using Temboo
        webView.getSettings().setJavaScriptEnabled(true);
        webView.loadUrl(intent.getStringExtra("url"));
        webView.setWebViewClient(new WebViewClient() {

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                //if the webpage matches that of the given url, then chop it up, get the code from temboo, and throw it to the Finilize oauthtask to run
                if(url.startsWith("https://msd88.temboolive.com/callback")) {
                    Log.d("Temboo", url);
                    String code = url.split("code=")[1];
                    Log.d("oAuthCode", code);

                    Intent intentBackToOnCall = new Intent(UserOauthActivity.this, OnCallActivity.class);

                    FinalizeOAuthTask finalizeOAuthTask = new FinalizeOAuthTask(UserOauthActivity.this, code, callbackId, intentBackToOnCall );
                    finalizeOAuthTask.execute();
                }
            }
        });
    }
}
