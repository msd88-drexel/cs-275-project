package edu.msdamoredrexel.csproject.googleoauth;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;

import com.temboo.Library.Google.OAuth.InitializeOAuth;
import com.temboo.core.TembooException;
import com.temboo.core.TembooSession;

/**
 * Created by damor_000 on 3/14/2015.
 */
public class InitilizeOauth extends AsyncTask<Void, Void, Void> {

    private Intent intent;
    private Context context;

    public InitilizeOauth(Intent intent, Context context) {
        this.intent = intent;
        this.context = context;
    }


    @Override
    protected Void doInBackground(Void... params) {
        try {
            // Instantiate the Choreo, using a previously instantiated TembooSession object, eg:
            TembooSession session = new TembooSession("msd88", "MyFirstApp", "738da4a6d1064ccfa72cacd3390547e9");

            InitializeOAuth initializeOAuthChoreo = new InitializeOAuth(session);

            // Get an InputSet object for the choreo
            InitializeOAuth.InitializeOAuthInputSet initializeOAuthInputs = initializeOAuthChoreo.newInputSet();

            initializeOAuthInputs.set_ClientID("19138628766-55kqe4ji1vhsm2c6smft6i6m29huik96.apps.googleusercontent.com");
            initializeOAuthInputs.set_Scope("https://www.googleapis.com/auth/calendar.readonly");

            InitializeOAuth.InitializeOAuthResultSet initializeOAuthResults = initializeOAuthChoreo.execute(initializeOAuthInputs);
            Log.d("OAuthUrl", initializeOAuthResults.get_AuthorizationURL());
            //Fill intent with information from the initialize oauth call
            intent.putExtra("url", initializeOAuthResults.get_AuthorizationURL());
            intent.putExtra("callbackId", initializeOAuthResults.get_CallbackID());

        } catch (TembooException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        intent.addFlags(Intent.FLAG_FROM_BACKGROUND);
        context.startActivity(intent);
    }

}
