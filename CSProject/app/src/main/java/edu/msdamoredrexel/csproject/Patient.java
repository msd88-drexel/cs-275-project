package edu.msdamoredrexel.csproject;

import com.cloudmine.api.db.LocallySavableCMObject;

import java.util.Date;

/**
 * Created by cheng on 3/15/2015.
 */
public class Patient extends LocallySavableCMObject {
    private String name_;
    private String symptom_  ;
    private String disability_;
    private String health_;
    private String generalhealthperceptions_;
    private String qualityoflife_;
    private String date_;

   public Patient(String Symptoms,String Disability,String Health,String Generalhealthperceptions,String Qualityoflife,String date,String name)
   {
       this.disability_ = Disability;
       this.generalhealthperceptions_ = Generalhealthperceptions;
       this.health_ = Health;
       this.qualityoflife_ = Qualityoflife;
       this.symptom_ = Symptoms;
       this.date_ = date;
       this.name_ = name;
   }
    //Adding empty constructor for cloudmine
    public Patient(){}

    public String getDisability_() { return this.disability_; }
    public String getHealth_() { return this.health_; }
    public String getSymptom_(){
        return this.symptom_;
    }
    public String getGeneralhealthperceptions_() { return this.generalhealthperceptions_; }
    public String getQualityoflife_() { return this.qualityoflife_; }
    public String getDate_() { return this.date_; }
    public String getName_() { return this.name_; }
}

