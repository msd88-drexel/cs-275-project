package edu.msdamoredrexel.csproject.googleoauth;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;

import com.temboo.Library.Google.OAuth.FinalizeOAuth;
import com.temboo.core.TembooException;
import com.temboo.core.TembooSession;

/**
 * Created by damor_000 on 3/14/2015.
 */
public class FinalizeOAuthTask extends AsyncTask<Void, Void, Void>{

    private static final String CLIENT_SECRET = "_FZSdbMTtwITfA0uVCrxG9-I";

    private String clientSecret;
    private String callbackId;
    private Context context;
    private Intent intent;

    public FinalizeOAuthTask(Context context, String clientSecret, String callbackId, Intent intent){
        this.clientSecret = clientSecret;
        this.callbackId = callbackId;
        this.intent = intent;
        this.context = context;
    }

    @Override
    protected Void doInBackground(Void... params) {
        try {
            // Instantiate the Choreo, using a previously instantiated TembooSession object, eg:
            TembooSession session = new TembooSession("msd88", "MyFirstApp", "738da4a6d1064ccfa72cacd3390547e9");

            FinalizeOAuth finalizeOAuthChoreo = new FinalizeOAuth(session);

          // Get an InputSet object for the choreo
            FinalizeOAuth.FinalizeOAuthInputSet finalizeOAuthInputs = finalizeOAuthChoreo.newInputSet();

            // Set inputs
            finalizeOAuthInputs.set_CallbackID(callbackId);
            finalizeOAuthInputs.set_ClientSecret(CLIENT_SECRET);
            finalizeOAuthInputs.set_ClientID("19138628766-55kqe4ji1vhsm2c6smft6i6m29huik96.apps.googleusercontent.com");

            // Execute Choreo
            FinalizeOAuth.FinalizeOAuthResultSet finalizeOAuthResults = finalizeOAuthChoreo.execute(finalizeOAuthInputs);
            intent.putExtra("accessToken", finalizeOAuthResults.get_AccessToken());
            Log.d("Access Token" , intent.getStringExtra("accessToken"));
        } catch (TembooException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        context.startActivity(intent);
    }
}
