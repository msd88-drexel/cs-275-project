package edu.msdamoredrexel.csproject.pathology;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.widget.ImageView;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import edu.msdamoredrexel.csproject.BitmapProcessingTask;

/**
 * Created by damor_000 on 3/16/2015.
 */
public class GetBitmapFromUrlTask extends AsyncTask<Void, Void, Bitmap>{

    String urlString;
    ImageView imageView;

    public GetBitmapFromUrlTask(String urlString, ImageView imageView){
        this.urlString = urlString;
        this.imageView = imageView;
    }


    @Override
    protected Bitmap doInBackground(Void... params) {

        try{
            URL url = new URL(urlString);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.connect();
            Bitmap image = BitmapFactory.decodeStream(connection.getInputStream());

            return image;
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    protected void onPostExecute(Bitmap imageResult) {
        super.onPostExecute(imageResult);
        BitmapProcessingTask.loadBitmap(imageResult, imageView);
    }

}